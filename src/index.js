import React from 'react';
import ReactDOM from 'react-dom';
import {createStore, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';
import {Provider} from 'react-redux';
import './css/index.css';
import allReducers from './js/reducers';
import App from './js/App';
import registerServiceWorker from './js/registerServiceWorker';

const store = createStore(allReducers, applyMiddleware(thunk));

const hasSessionId = localStorage.getItem('sessionId') != '' && localStorage.getItem('sessionId') != null;

if (hasSessionId) {
  store.dispatch({ type: 'IS_LOGGED_IN'});
}

ReactDOM.render(
  <Provider store={store}>
    < App />
  </ Provider>, document.getElementById('root'));

registerServiceWorker();
