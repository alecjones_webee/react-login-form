import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import LoginForm from './LoginForm';
import ValidationPopup from './template/ValidationPopup';
import Flag from 'react-flags';
import { Overlay } from 'react-bootstrap';
import {connect} from 'react-redux';
import {languageSelect} from './actions/index';

import '../css/LoginPage.css';

class HomeScreen extends Component {
  constructor() {
    super();
    this.state = {
      bgArray: ['bg', 'bg1', 'bg2', 'bg3', 'bg4'],
      bg: 'bg',
      i: 0,
      lang: 'es',
      show: true,
    }
  }

  componentDidMount() {
    this.changeBg();
  }

  changeBg() {
    let i = 0;
    setInterval(function() {
      i = Math.floor(Math.random()*5);
      this.setState({
        bg: this.state.bgArray[i]
      });
    }.bind(this), 5000);
  }

  changeLang(l) {
      this.setState({
        lang: l
      });
  }

  render() {
    console.log(this.state.language);
    return (
      <div>

        <div className={this.state.bg}></div>
        <div className="filter"></div>
        <div className="login-page">
          <div className="logo"></div>
          <div className="logo-text">
            Homepage you are now login.
          </div>
        </div>
      </div>
    );
  }
}

export default connect(state => ({language: state.lang }), {languageSelect})(HomeScreen);
