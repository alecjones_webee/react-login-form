import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import LoginForm from './LoginForm';
import ValidationPopup from './template/ValidationPopup';
import Flag from 'react-flags';
import { Overlay } from 'react-bootstrap';
import {connect} from 'react-redux';
import {languageSelect} from './actions/index';

import '../css/LoginPage.css';

class LoginScreen extends Component {
  constructor() {
    super();
    this.state = {
      bgArray: ['bg', 'bg1', 'bg2', 'bg3', 'bg4'],
      bg: 'bg',
      i: 0,
      lang: 'es',
      show: true,
    }
  }

  componentDidMount() {
    this.changeBg();
  }

  changeBg() {
    let i = 0;
    setInterval(function() {
      i = Math.floor(Math.random()*5);
      this.setState({
        bg: this.state.bgArray[i]
      });
    }.bind(this), 5000);
  }

  changeLang(l) {
      this.setState({
        lang: l
      });
  }



  render() {
    console.log(this.state.language);
    return (
      <div>

        <div className={this.state.bg}></div>
        <div className="filter"></div>
        <div className="login-page">
          <div className="logo"></div>
          <div className="logo-text">
            Gana dinero diseñado experiencias únicas para las personas que visitan tu ciudad
          </div>
          <div className="languages-pane">
            <button title="English" onClick={() => this.props.languageSelect('en')}>
              <Flag name="GB" format="png" pngSize={24} shiny={false} alt="English" />
            </button>
            <button title="Spanish" onClick={() => this.props.languageSelect('es')}>
              <Flag name="ES" format="png" pngSize={24} shiny={false} alt="Spanish" />
            </button>
          </div>
          <div className="login-form-container" ref="target">

            <LoginForm history={this.props.history} />
          </div>
        </div>
      </div>
    );
  }
}

export default connect(state => ({language: state.lang.language, isLoggedIn : state.lang.isLoggedIn }), {languageSelect})(LoginScreen);
