const initialState = { language : 'en', isLoggedIn : false};
export default function(state = initialState, action){
  switch (action.type) {
    case 'LANGUAGE_SELECT':
        return { language: action.payload, isLoggedIn: state.isLoggedIn }
      break;
    case 'IS_LOGGED_IN':
        return { language: state.language, isLoggedIn: true }
      break;
  }
  return state;
}
