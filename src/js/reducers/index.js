import {combineReducers} from 'redux';
import {reducer as reduxFormReducer} from 'redux-form';
import languageReducer from './LanguageReducer'

const allReducers = combineReducers({
  form: reduxFormReducer,
  lang: languageReducer
});

export default allReducers;
