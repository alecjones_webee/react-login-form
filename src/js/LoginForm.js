import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';
import {connect} from 'react-redux';
import _ from 'lodash';

class LoginForm extends Component{
  constructor(props){
    super(props);
    this.onSubmit = this.onSubmit.bind(this);
    this.socialMediaLogin = this.socialMediaLogin.bind(this);
    this.state = { hasError : false , errorMessage : ''};
  }

  onSubmit(values) {
    if ( values.email === 'ajn@mail.com' ){
      localStorage.setItem('sessionId', values.email);
      this.props.history.push('/home');
    }
  }

  socialMediaLogin() {
    alert("Not yet implemented: "+this.state.hasError);
  }

  checkError(){
    const {loginForm, valid} = this.props;

    if(_.has(loginForm, 'fields')  && _.has(loginForm, 'values')){
      if (_.has(loginForm, 'syncErrors'))
        this.setState({hasError : true});
      else
        this.setState({hasError : false});
    }

    this.state.errorMessage = this.getErrorMessage();
    console.log('Check');
  }

  showError(){
    const {loginForm, valid} = this.props;

    if(this.state.hasError && this.state.errorMessage !== '')
      return (
        <div className="login-alert-dialog">
          <div className="dialog-content-wrapper">
            <i className="fa fa-exclamation-triangle" aria-hidden="true"></i>
            <p>{this.state.errorMessage}</p>
            <a className="close" onClick={() => this.hideDialog()}><i className="fa fa-times" aria-hidden="true"></i></a>
          </div>
        </div>
      );
  }

  getErrorMessage(){
    const {loginForm} = this.props;
    let emailError = '';
    let passwordError = '';

    if(_.has(loginForm, 'fields')){
      if (_.has(loginForm, 'syncErrors.email'))
        if ( _.has(loginForm, 'values.email'))
          emailError = _.get(loginForm, 'syncErrors.email');
    }

    if(_.has(loginForm, 'fields')){
      if (_.has(loginForm, 'syncErrors.password'))
        if (_.has(loginForm, 'values.password') || _.get(loginForm, 'fields.password.visited'))
          passwordError = _.get(loginForm, 'syncErrors.password');
    }

    return emailError+passwordError;
  }

  hideDialog(){
    this.setState({hasError : false});
  }

  render() {
    const { handleSubmit, pristine, reset, submitting, loginForm, valid} = this.props
    return (
      <div className="login-wrapper">
      {this.showError()}
      <form onSubmit={handleSubmit(this.onSubmit)} className="form-horizontal">
        <div className="login-form-group form-group">
          <Field name="email" type="email" component="input" onBlur={() => this.checkError()} label="Email" className="form-control" placeholder={this.props.language === 'es'? 'Email es' : 'Email'}/>
          <Field name="password" type="password" component="input" onBlur={() => this.checkError()} label="Password" className="form-control" placeholder={this.props.language === 'es'? 'Contraseña' : 'Password'}/>
        </div>

        <div className="login-form-group form-group">
          <button type="submit" disabled={!valid} className="btn btn-primary btn-block">
            {this.props.language === 'es' ? 'Registrame' : 'Register'}
          </button>
        </div>

        <div className="login-form-group form-group">
          <h4>
          <span>
            {this.props.language === 'es'? 'O registrate con tus redes favoritas': 'Or you can sign up with your favorite networks'}
          </span>
          </h4>
        </div>

        <div className="login-form-group form-group">
          <div className="btn-group">
            <button type="button" onClick={() => this.socialMediaLogin()} className="facebook-blue btn btn-default"><i className="fa fa-facebook"></i> Facebook</button>
            <button type="button" onClick={() => this.socialMediaLogin()} className="twitter-blue btn btn-default"><i className="fa fa-twitter"></i> Twitter</button>
            <button type="button" onClick={() => this.socialMediaLogin()} className="google-red btn btn-default"><i className="fa fa-google"></i> Google</button>
          </div>
        </div>
      </form>
      </div>
    );
  }
}

const validate = values => {
  const errors = {}
  const regex = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i;
  if (!values.email) {
    errors.email = 'Required'
  } else{
    if(!regex.test(values.email))
      errors.email = 'Tu email ya esta dado de alta en Tripbru Accede a tu cuenta'
  }

  if (!values.password) {
    errors.password = 'Password Required'
  }
  // else{
  //   let passwordErrors = '';
  //
  //   if(values.password.length < 6)
  //     passwordErrors += 'Needs to be atleast 6 characters.';
  //
  //   if(!/[A-Z]/.test(values.password))
  //     passwordErrors += ' Needs at least one uppercase letter.';
  //
  //   if(!/[a-z]/.test(values.password))
  //     passwordErrors += ' Needs at least one lowercase letter.';
  //
  //   if(!/[0-9]/.test(values.password))
  //     passwordErrors += ' Needs at least one number.';
  //
  //   if(passwordErrors != '')
  //     errors.password = passwordErrors;
  // }

  return errors
}

const warn = values => {
  const warnings = {}
  if (values.age < 19) {
    warnings.age = 'Hmm, you seem a bit young...'
  }
  return warnings
}

LoginForm = reduxForm({
  form: 'loginForm',
  validate,
  warn
})(LoginForm);

LoginForm = connect(
  state => ({loginForm : state.form.loginForm, language: state.lang}), null)(LoginForm);

export default LoginForm;
