export const languageSelect = (language) => {
  return{
    type: 'LANGUAGE_SELECT',
    payload: language
  }
};
